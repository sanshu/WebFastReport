unit ReportData;

interface
uses
  SysUtils, Variants, Classes,DBClient,NativeXml,DB,SysComm;
Type
  PVariable = ^RVariable;
  RVariable = record
    key:String;
    value:String;
  end;
Type
  PDataSet = ^RDataSet;
  RDataSet = record
    name:String;
    dataset:TClientDataSet;
  end;
Type
  TReportData = class
  private
    owner:TComponent;
    variables:TList;
    datasets:TList;
  public
    constructor create(AOwner: TComponent);
    destructor destroy;
    procedure load(stream:TStream);
    procedure put(key:String;value:String);overload;
    procedure put(datasetName:String;dataset:TClientDataSet);overload;
    function getVariable(key:String):String;overload;
    function getVariable(index:Integer):String;overload;
    function getRVariable(index:Integer):RVariable;
    function getDataSet(datasetName:String):TClientDataSet;overload;
    function getDataSet(index:Integer):TClientDataSet;overload;
    function getRDataSet(index:Integer):RDataSet;
    function getVariableCount():Integer;
    function getDataSetCount():Integer;
end;
implementation

{ TReportData }

procedure TReportData.put(key, value: String);
var
  pv:PVariable;
begin
  New(pv);
  pv^.key:=key;
  pv^.value:=value;
  self.variables.Add(pv);
end;
procedure TReportData.put(datasetName: String; dataset: TClientDataSet);
var
  pd:PDataSet;
begin
  New(pd);
  pd^.name := datasetName;
  pd^.dataset := dataset;
  Self.datasets.Add(pd);
end;

function TReportData.getVariable(key: String): String;
var
  i:Integer;
  r:RVariable;
begin
  for i := 0  to self.variables.Count-1 do
  begin
     if PVariable(self.variables[i])^.key = key then
     begin
       r:=PVariable(self.variables[i])^;
     end;
  end;
  result:=r.value;
end;

function TReportData.getDataSet(datasetName: String): TClientDataSet;
var
  i:Integer;
  r:RDataSet;
begin
   for i := 0 to self.datasets.Count-1 do
   begin
      if PDataSet(self.datasets[i])^.name=datasetName then
      begin
        r:=PDataSet(self.datasets[i])^;
      end;
   end;
   result:=r.dataset;
end;

constructor TReportData.create(AOwner: TComponent);
begin
  Self.owner:=AOwner;
  self.variables:=TList.Create;
  self.datasets:=TList.Create;
end;

destructor TReportData.destroy;
var
  i:Integer;
  r:RDataSet;
begin
  FreeAndNil(Self.variables);
  for i := 0 to self.datasets.Count-1 do
  begin
     r:=PDataSet(self.datasets[i])^;
     FreeAndNil(r.dataset);
  end;
  FreeAndNil(self.datasets);
end;

procedure TReportData.load(stream: TStream);
var
  i,j,h:Integer;
  k,v:String;
  xml:TNativeXml;
  node,dsNode,dsfieldsNode,fieldNode,dsrowdataNode,dsrowNode:TXmlNode;
  ds:TClientDataSet;
  dsName:string;
  fieldName,fieldType,fieldValue:string;
  fieldWidth:Integer;
  AFloatValue:Double;
  AIntValue:Integer;
  ADateTimeValue:TDateTime;
begin
  xml:=TNativeXml.Create;
  xml.EncodingString :='Utf-8';
  xml.LoadFromStream(stream);
  //解析变量
  node:=xml.Root.FindNode('variables');
  for i :=0  to node.NodeCount-1 do
  begin
     k:=node.Nodes[i].AttributeName[0];
     v:=node.Nodes[i].AttributeValue[0];
     self.put(k,v);
  end;
  //解析数据集 (支持多个数据集解析)
  node:=xml.Root.FindNode('datasets');
  for i:=0 to node.NodeCount-1 do
  begin
    //每个数据集节点由一个TClientDataSet存储
    ds:=TClientDataSet.Create(self.owner);
    ds.FieldDefs.Clear;

    dsNode:=node.Nodes[i];                                                      {取得一个数据集节点}
    dsName:=dsNode.AttributeByName['name'];                                     {取得数据集的名称}
    dsfieldsNode:=dsnode.FindNode('metadata').FindNode('fields');               {取得数据集的列属性}
    //解析字段
    for  j:=0  to dsfieldsNode.NodeCount-1 do
    begin    
      fieldNode:=dsfieldsNode[j];                                               {取得字段定义节点}
      fieldName:=fieldNode.AttributeByName['attrname'];                         {取得字段定义属性:字段名称}
      fieldType:=TSysCommon.LowerCase(fieldNode.AttributeByName['fieldtype']);  {取得字段定义属性:字段类型}
      fieldWidth:=StrToInt(fieldNode.AttributeByName['width']);                 {取得字段定义属性:字段长度}
      {根据字段定义属性，定义TClientDataSet列集合}
      if fieldType = 'string' then
          ds.FieldDefs.Add(fieldName,ftString,fieldWidth)
      else  if fieldType = 'wstring' then
          ds.FieldDefs.Add(fieldName,ftWideString,fieldWidth)
      else  if fieldType = 'float'  then
          ds.FieldDefs.Add(fieldName,ftFloat)
      else if fieldType = 'datetime' then
          ds.FieldDefs.Add(fieldName,ftDateTime)
      else if fieldType = 'integer' then
          ds.FieldDefs.Add(fieldName,ftInteger)
      else if fieldType = 'memo' then
          ds.FieldDefs.Add(fieldName,ftMemo)
      else
          ds.FieldDefs.Add(fieldName,ftString,1)
    end;
    {解析数据}
    dsrowdataNode:=dsnode.FindNode('rowdata');
    ds.CreateDataSet;                                                           {创建新的数据集}
    ds.Active;                                                                  {激活}
    for  j:=0  to dsrowdataNode.NodeCount-1  do                                 {遍历数据行}
    begin
      dsrowNode:= dsrowdataNode[j];                                             {取得一行}
      ds.Append;                                                                {打开追加控制}

      for h:=0 to ds.Fields.Count-1 do
      begin
          fieldValue:=dsrowNode.AttributeByName[ds.Fields[h].FieldName];
          if fieldValue = '' then
          begin
              ds.Fields[h].Clear;
          end
          else
          case ds.Fields[h].DataType of
              ftString     : ds.Fields[h].AsString := fieldValue;
              ftWideString : ds.Fields[h].AsString := fieldValue;
              ftFloat      : if TryStrToFloat(fieldValue,AFloatValue) then ds.Fields[h].AsFloat := AFloatValue;
              ftInteger    : if TryStrToInt(fieldValue,AIntValue) then ds.Fields[h].AsInteger := AIntValue;
              ftDataSet    : if TryStrToDateTime(fieldValue,ADateTimeValue) then ds.Fields[h].AsDateTime := ADateTimeValue;
              ftMemo       : ds.Fields[h].AsString := fieldValue;
           end;
      end;
    end;
    ds.Post;
    ds.Open;
    Self.put(dsName,ds);
  end;
  FreeAndNil(xml);
end;

function TReportData.getDataSetCount: Integer;
begin
  Result:=self.datasets.Count;
end;

function TReportData.getVariableCount: Integer;
begin
  Result:=self.variables.Count;
end;

function TReportData.getDataSet(index: Integer): TClientDataSet;
begin
  Result:=PDataSet(Self.datasets.Items[index])^.dataset;
end;

function TReportData.getVariable(index: Integer): String;
begin
  Result:=PVariable(Self.variables.Items[index])^.value;
end;

function TReportData.getRDataSet(index: Integer): RDataSet;
begin
  Result:=PDataSet(Self.datasets.Items[index])^;
end;

function TReportData.getRVariable(index: Integer): RVariable;
begin
  Result:=PVariable(Self.variables.Items[index])^;
end;
end.

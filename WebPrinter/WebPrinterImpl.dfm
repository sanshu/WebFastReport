object WebPrinter: TWebPrinter
  Left = 379
  Top = 237
  Width = 419
  Height = 118
  Caption = 'WebPrinter'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = ActiveFormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    PrintOptimized = False
    Outline = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    Background = False
    Creator = 'FastReport (http://www.fast-report.com)'
    HTMLTags = True
    Left = 40
    Top = 24
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 80
    Top = 24
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    ShowProgress = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    Left = 120
    Top = 24
  end
  object frxCSVExport1: TfrxCSVExport
    UseFileCache = True
    ShowProgress = True
    Separator = ';'
    OEMCodepage = False
    Left = 160
    Top = 24
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 208
    Top = 24
  end
  object frxOLEObject1: TfrxOLEObject
    Left = 248
    Top = 24
  end
  object frxRichObject1: TfrxRichObject
    Left = 288
    Top = 24
  end
  object frxChartObject1: TfrxChartObject
    Left = 336
    Top = 24
  end
end
